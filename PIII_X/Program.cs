﻿using System;
using System.IO;
using System.Linq;

namespace PIII_X
{
    class Program
    {
        static void Main(string[] args)
        {
            var lines = File.ReadAllLines(Environment.CurrentDirectory +"/data");
            var people = lines.Select(x =>
            {
                string[] data = x.Split(',');
                return new Person(Convert.ToInt32(data[0]),data[1],data[2],data[3]);
            });

            var sortedPeople = people.OrderBy(x => x.lastName).ThenBy(x => x.firstName);

            using (StreamWriter writer = new StreamWriter("results"))
            {
                foreach (var item in sortedPeople)
                {
                    writer.WriteLine($"[{item.id}] {item.firstName} {item.lastName}: {item.phone}");
                }
            }
            
            
            
        }
    }
}
