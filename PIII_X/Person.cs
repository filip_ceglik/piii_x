namespace PIII_X
{
    public class Person
    {
        public int id;
        public string firstName, lastName, phone;
        

        public Person(int id, string firstName, string lastName, string phone)
        {
            this.id = id;
            this.firstName = firstName;
            this.lastName = lastName;
            this.phone = phone;
        }
    }
}